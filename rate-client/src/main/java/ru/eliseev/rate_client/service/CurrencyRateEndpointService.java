package ru.eliseev.rate_client.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.eliseev.rate_client.model.CurrencyRate;
import ru.eliseev.rate_client.clients.RateClient;
import ru.eliseev.rate_client.model.RateType;

import java.time.LocalDate;
import java.util.Map;

@Service
@Slf4j
public class CurrencyRateEndpointService {
    private final Map<String, RateClient> clients;

    @Autowired
    public CurrencyRateEndpointService(Map<String, RateClient> clients) {
        this.clients = clients;
    }

    public CurrencyRate getCurrentRate(RateType rateType, String currency, LocalDate date) {
        log.info("CLIENT_SERVICE.getCurrencyRate(), rateType:{} currency:{}, date:{}", rateType, currency, date);
        var client = clients.get(rateType.getServiceName());
        return client.getCurrencyRate(currency, date);
    }
}
