package ru.eliseev.rate_client.model.exceptions;

public class HttpClientException extends RuntimeException {
    public HttpClientException(String msg) {
        super(msg);
    }
}
