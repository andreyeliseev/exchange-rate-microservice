package ru.eliseev.rate_client.model.exceptions;

public class RateClientException extends RuntimeException {
    public RateClientException(String msg) {
        super(msg);
    }
}
