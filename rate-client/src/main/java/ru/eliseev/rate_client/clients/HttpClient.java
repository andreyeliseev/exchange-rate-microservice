package ru.eliseev.rate_client.clients;

public interface HttpClient {
    String performRequest(String url);
}
