package ru.eliseev.rate_client.clients;

import ru.eliseev.rate_client.model.CurrencyRate;

import java.time.LocalDate;

public interface RateClient {
    CurrencyRate getCurrencyRate(String currency, LocalDate date);
}
