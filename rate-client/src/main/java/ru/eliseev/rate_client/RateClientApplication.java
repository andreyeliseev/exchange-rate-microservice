package ru.eliseev.rate_client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RateClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(RateClientApplication.class, args);
	}

}
