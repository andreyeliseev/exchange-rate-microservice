package ru.eliseev.rate_bot.model.exception;

import com.fasterxml.jackson.core.JsonProcessingException;

public class TelegramException extends RuntimeException {
    public TelegramException(Throwable cause) {
        super(cause);
    }

    public TelegramException(String message) {
        super(message);
    }
}
