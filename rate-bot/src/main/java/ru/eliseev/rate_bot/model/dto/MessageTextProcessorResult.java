package ru.eliseev.rate_bot.model.dto;

import lombok.Value;

@Value
public class MessageTextProcessorResult {
    String okReply;
    String failReply;
}
