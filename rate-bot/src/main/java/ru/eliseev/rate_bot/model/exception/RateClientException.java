package ru.eliseev.rate_bot.model.exception;

public class RateClientException extends RuntimeException {
    public RateClientException(String message) {
        super(message);
    }
}
