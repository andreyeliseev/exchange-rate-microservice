package ru.eliseev.rate_bot.service;

import java.time.LocalDateTime;

public interface DateTimeProvider {
    LocalDateTime get();
}
