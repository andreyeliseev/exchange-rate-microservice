package ru.eliseev.rate_bot.service;

public interface LastUpdateIdKeeper {
    long get();

    void set(long lastUpdateId);
}
