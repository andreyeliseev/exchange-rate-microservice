package ru.eliseev.rate_bot.service.processor;

import ru.eliseev.rate_bot.model.dto.MessageTextProcessorResult;

public interface MessageTextProcessor {
    MessageTextProcessorResult process(String message);
}
