package ru.eliseev.rate_bot.service.processor;

import org.springframework.stereotype.Service;
import ru.eliseev.rate_bot.model.dto.MessageTextProcessorResult;

import static ru.eliseev.rate_bot.service.processor.Messages.EXPECTED_FORMAT_MESSAGE;

@Service("messageTextProcessorStart")
public class MessageTextProcessorStart implements MessageTextProcessor {
    @Override
    public MessageTextProcessorResult process(String message) {
        return new MessageTextProcessorResult(EXPECTED_FORMAT_MESSAGE.getText(), null);
    }
}
