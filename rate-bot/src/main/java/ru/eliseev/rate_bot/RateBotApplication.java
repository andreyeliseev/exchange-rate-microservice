package ru.eliseev.rate_bot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RateBotApplication {

	public static void main(String[] args) {
		SpringApplication.run(RateBotApplication.class, args);
	}

}
