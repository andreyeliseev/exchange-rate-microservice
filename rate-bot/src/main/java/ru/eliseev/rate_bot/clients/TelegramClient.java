package ru.eliseev.rate_bot.clients;

import ru.eliseev.rate_bot.model.dto.GetUpdatesRequest;
import ru.eliseev.rate_bot.model.dto.GetUpdatesResponse;
import ru.eliseev.rate_bot.model.dto.SendMessageRequest;

public interface TelegramClient {
    GetUpdatesResponse getUpdates(GetUpdatesRequest request);
    void sendMessage(SendMessageRequest request);
}
