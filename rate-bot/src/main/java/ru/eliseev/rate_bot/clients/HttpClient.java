package ru.eliseev.rate_bot.clients;

public interface HttpClient {
    String performRequest(String url, String params);
    String performRequest(String url);
}
