package ru.eliseev.rate_bot.clients;

import ru.eliseev.rate_bot.model.Rate;

import java.time.LocalDate;

public interface RateClient {
    Rate getRate(String rateType, String currency, LocalDate date);
}
