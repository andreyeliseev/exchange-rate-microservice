package ru.eliseev.rate_bot.clients;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.eliseev.rate_bot.config.RateClientConfig;
import ru.eliseev.rate_bot.model.Rate;
import ru.eliseev.rate_bot.model.exception.HttpClientException;
import ru.eliseev.rate_bot.model.exception.RateClientException;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Service
@RequiredArgsConstructor
@Slf4j
public class RateClientImpl implements RateClient {
    public static final String DATE_FORMAT = "dd-MM-yyyy";
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern(DATE_FORMAT);

    private final RateClientConfig config;
    private final ru.eliseev.rate_bot.clients.HttpClient httpClient;
    private final ObjectMapper objectMapper;

    @Override
    public Rate getRate(String rateType, String currency, LocalDate date) {
        log.info("BOT_TO_CLIENT.getRate(), rateType:{}, currency:{}, date:{}", rateType, currency, date);
        var urlWithParams = String.format("%s/%s/%s/%s", config.getUrl(), rateType, currency, DATE_FORMATTER.format(date));

        try {
            var response = httpClient.performRequest(urlWithParams);
            return objectMapper.readValue(response, Rate.class);
        } catch (HttpClientException ex) {
            throw new RateClientException("Error from Client host:" + ex.getMessage());
        } catch (Exception ex) {
            log.error("Getting currencyRate error, currency:{}, date:{}", currency, date, ex);
            throw new RateClientException("Can't get currencyRate. currency:" + currency + ", date:" + date);
        }
    }
}
