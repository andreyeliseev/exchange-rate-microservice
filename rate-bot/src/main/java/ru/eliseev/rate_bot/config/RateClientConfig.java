package ru.eliseev.rate_bot.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "rate-client")
public class RateClientConfig {
    String url;
}
