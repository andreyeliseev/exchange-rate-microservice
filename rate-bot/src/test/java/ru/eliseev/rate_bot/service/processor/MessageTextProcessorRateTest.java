package ru.eliseev.rate_bot.service.processor;

import org.junit.jupiter.api.Test;
import ru.eliseev.rate_bot.clients.RateClient;
import ru.eliseev.rate_bot.model.Rate;
import ru.eliseev.rate_bot.model.dto.MessageTextProcessorResult;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class MessageTextProcessorRateTest {
    @Test
    void processTestAgrs3() {
        //given
        var rateClient = mock(RateClient.class);
        var rate = new Rate("USD", "1", "49.4");
        when(rateClient.getRate("CBR", "USD", LocalDate.of(2021, 2, 3)))
                .thenReturn(rate);

        var messageTextProcessor = new MessageTextProcessorRate(rateClient,
                () -> LocalDateTime.of(2021, 2, 3, 1, 1, 1));
        var msg = "CBR USD 3-02-2021";

        //when
        var result = messageTextProcessor.process(msg);

        //then
        assertThat(result.getFailReply()).isNull();
        assertThat(result.getOkReply()).isEqualTo(rate.getValue());
    }

    @Test
    void processTestAgrs2() {
        //given
        var rateClient = mock(RateClient.class);
        var rate = new Rate("USD", "1", "49.4");
        when(rateClient.getRate("CBR", "USD", LocalDate.of(2021, 2, 3)))
                .thenReturn(rate);

        var messageTextProcessor = new MessageTextProcessorRate(rateClient,
                () -> LocalDateTime.of(2021, 2, 3, 1, 1, 1));
        var msg = "USD 03-02-2021";

        //when
        var result = messageTextProcessor.process(msg);

        //then
        assertThat(result.getFailReply()).isNull();
        assertThat(result.getOkReply()).isEqualTo(rate.getValue());
    }

    @Test
    void processTestAgrs1() {
        //given
        var rateClient = mock(RateClient.class);
        var rate = new Rate("USD", "1", "49.4");
        var resultExpected = new MessageTextProcessorResult(rate.getValue(), null);
        when(rateClient.getRate("CBR", "USD", LocalDate.of(2021, 2, 3)))
                .thenReturn(rate);

        var messageTextProcessor = new MessageTextProcessorRate(rateClient,
                () -> LocalDateTime.of(2021, 2, 3, 1, 1, 1));
        var msg = "USD";

        //when
        var result = messageTextProcessor.process(msg);

        //then
        assertThat(result.getFailReply()).isNull();
        assertThat(result.getOkReply()).isEqualTo(rate.getValue());
    }

    @Test
    void processTestAgrsWrongData() {
        //given
        var rateClient = mock(RateClient.class);
        var rate = new Rate("USD", "1", "49.4");
        when(rateClient.getRate("CBR", "USD", LocalDate.of(2021, 2, 3)))
                .thenReturn(rate);

        var messageTextProcessor = new MessageTextProcessorRate(rateClient,
                () -> LocalDateTime.of(2021, 2, 3, 1, 1, 1));
        var msg = "USD 2021.03.01";

        //when
        var result = messageTextProcessor.process(msg);

        //then
        assertThat(result.getOkReply()).isNull();
        assertThat(result.getFailReply()).isEqualTo(Messages.DATA_FORMAT_MESSAGE.getText());
    }

    @Test
    void processTestAgrsWrongFormat() {
        //given
        var rateClient = mock(RateClient.class);
        var rate = new Rate("USD", "1", "49.4");
        when(rateClient.getRate("CBR", "USD", LocalDate.of(2021, 2, 3)))
                .thenReturn(rate);

        var messageTextProcessor = new MessageTextProcessorRate(rateClient,
                () -> LocalDateTime.of(2021, 2, 3, 1, 1, 1));
        var msg = "RRR RRR USD 03-02-2021";

        //when
        var result = messageTextProcessor.process(msg);

        //then
        assertThat(result.getOkReply()).isNull();
        assertThat(result.getFailReply()).isEqualTo(Messages.EXPECTED_FORMAT_MESSAGE.getText());
    }
}