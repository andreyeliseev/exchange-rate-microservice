package ru.eliseev.cbr_rate.parser;

import org.junit.jupiter.api.Test;
import ru.eliseev.cbr_rate.model.CbrRate;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class CbrRateParserXmlTest {

    @Test
    void parse() throws URISyntaxException, IOException {
        //given
        var parser = new CbrRateParserXml();
        var uri = ClassLoader.getSystemResource("example.xml").toURI();
        var ratesXML = Files.readString(Paths.get(uri), Charset.forName("Windows-1251"));

        //when
        var rates = parser.parse(ratesXML);

        //then
        assertThat(rates.size()).isEqualTo(34);
        assertThat(rates.contains(getUSDrate())).isTrue();
        assertThat(rates.contains(getEURrate())).isTrue();
        assertThat(rates.contains(getJPYrate())).isTrue();
    }

    CbrRate getUSDrate() {
        return CbrRate.builder()
                .numCode("840")
                .charCode("USD")
                .nominal("1")
                .name("Доллар США")
                .value("59,6663")
                .build();
    }

    CbrRate getEURrate() {
        return CbrRate.builder()
                .numCode("978")
                .charCode("EUR")
                .nominal("1")
                .name("Евро")
                .value("59,6196")
                .build();
    }

    CbrRate getJPYrate() {
        return CbrRate.builder()
                .numCode("392")
                .charCode("JPY")
                .nominal("100")
                .name("Японских иен")
                .value("41,6519")
                .build();
    }
}