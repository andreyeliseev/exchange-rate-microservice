package ru.eliseev.cbr_rate.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import ru.eliseev.cbr_rate.config.ApplicationConfig;
import ru.eliseev.cbr_rate.config.CbrConfig;
import ru.eliseev.cbr_rate.config.JsonConfig;
import ru.eliseev.cbr_rate.parser.CbrRateParserXml;
import ru.eliseev.cbr_rate.requester.CbrRequester;
import ru.eliseev.cbr_rate.services.CbrRateService;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.eliseev.cbr_rate.services.CbrRateService.DATE_FORMAT;

@WebMvcTest(CbrRateController.class)
@Import({ApplicationConfig.class, JsonConfig.class, CbrRateService.class, CbrRateParserXml.class})
public class CbrRateControllerTest {
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern(DATE_FORMAT);
    @Autowired
    MockMvc mockMvc;

    @Autowired
    CbrConfig cbrConfig;

    @MockBean
    CbrRequester cbrRequester;

    @Test
    @DirtiesContext
    void getCurrencyRateTest() throws Exception {
        //given
        var currency = "EUR";
        var date = "16-09-2022";
        prepareCbrRequestMock(date);

        //when
        var result = mockMvc.perform(get(String.format("/api/v1/exchangeRate/%s/%s", currency, date)))
                .andExpect(status().isOk())
                .andReturn().getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        assertThat(result).isEqualTo("{\"numCode\":\"978\",\"charCode\":\"EUR\",\"nominal\":\"1\",\"name\":\"Евро\",\"value\":\"59,6196\"}");
    }


    @Test
    @DirtiesContext
    void cacheUseTest() throws Exception {
        //given
        prepareCbrRequestMock(null);

        var currency = "EUR";
        var date = "16-09-2022";

        //when
        mockMvc.perform(get(String.format("/api/v1/exchangeRate/%s/%s", currency, date))).andExpect(status().isOk());
        mockMvc.perform(get(String.format("/api/v1/exchangeRate/%s/%s", currency, date))).andExpect(status().isOk());

        currency = "USD";
        mockMvc.perform(get(String.format("/api/v1/exchangeRate/%s/%s", currency, date))).andExpect(status().isOk());

        date = "15-09-2022";
        mockMvc.perform(get(String.format("/api/v1/exchangeRate/%s/%s", currency, date))).andExpect(status().isOk());

        verify(cbrRequester, times(2)).getRatesAsXml(any());
    }

    private void prepareCbrRequestMock(String date) throws URISyntaxException, IOException {
        var uri = ClassLoader.getSystemResource("example.xml").toURI();
        var ratesXml = Files.readString(Paths.get(uri), Charset.forName("Windows-1251"));

        if (date == null) {
            when(cbrRequester.getRatesAsXml(any())).thenReturn(ratesXml);
        } else {
            var dateParam = LocalDate.parse(date, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
            var cbrUrl = String.format("%s?date_req=%s", cbrConfig.getUrl(), DATE_FORMATTER.format(dateParam));
            when(cbrRequester.getRatesAsXml(cbrUrl)).thenReturn(ratesXml);
        }
    }
}
