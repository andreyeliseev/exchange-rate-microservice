package ru.eliseev.cbr_rate.parser;

import ru.eliseev.cbr_rate.model.CbrRate;

import java.util.List;

public interface CbrRateParser {
    List<CbrRate> parse(String ratesAsString);
}
