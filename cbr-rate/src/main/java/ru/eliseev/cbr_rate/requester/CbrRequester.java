package ru.eliseev.cbr_rate.requester;

public interface CbrRequester {
    String getRatesAsXml(String url);
}
