package ru.eliseev.cbr_rate.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ehcache.Cache;
import org.springframework.stereotype.Service;
import ru.eliseev.cbr_rate.config.CbrConfig;
import ru.eliseev.cbr_rate.model.CachedCbrRates;
import ru.eliseev.cbr_rate.model.CbrRate;
import ru.eliseev.cbr_rate.model.exceptions.CbrRateNotFoundException;
import ru.eliseev.cbr_rate.parser.CbrRateParser;
import ru.eliseev.cbr_rate.requester.CbrRequester;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class CbrRateService {
    public static final String DATE_FORMAT = "dd/MM/yyyy";
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern(DATE_FORMAT);

    private final CbrRequester cbrRequester;
    private final CbrRateParser cbrRateParser;
    private final CbrConfig cbrConfig;
    private final Cache<LocalDate, CachedCbrRates> currencyRatesCache;

    public CbrRate getCurrencyRate(String currency, LocalDate date) {
        log.info("CBR_SERVICE.getCurrencyRate(). currency:{}, date:{}", currency, date);
        List<CbrRate> rates;

        var cachedCurrencyRates = currencyRatesCache.get(date);
        if (cachedCurrencyRates == null) {
            var urlWithParams = String.format("%s?date_req=%s", cbrConfig.getUrl(), DATE_FORMATTER.format(date));
            var ratesAsXml = cbrRequester.getRatesAsXml(urlWithParams);
            rates = cbrRateParser.parse(ratesAsXml);
            currencyRatesCache.put(date, new CachedCbrRates(rates));
        } else {
            rates = cachedCurrencyRates.getCbrRates();
        }

        return rates.stream().filter(rate -> currency.equals(rate.getCharCode()))
                .findFirst()
                .orElseThrow(() -> new CbrRateNotFoundException("Cbr Rate not found. Currency:" + currency + ". Date" + date));
    }
}
