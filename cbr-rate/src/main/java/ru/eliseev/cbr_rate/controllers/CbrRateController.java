package ru.eliseev.cbr_rate.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.eliseev.cbr_rate.model.CbrRate;
import ru.eliseev.cbr_rate.services.CbrRateService;

import java.time.LocalDate;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping(path = "${app.rest.api.prefix}/v1")
public class CbrRateController {

    public final CbrRateService cbrRateService;

    @GetMapping("/exchangeRate/{currency}/{date}")
    public CbrRate getCurrencyRate(@PathVariable("currency") String currency,
                                   @DateTimeFormat(pattern = "dd-MM-yyyy") @PathVariable("date") LocalDate date) {
        log.info("CBR_CONTROLLER.getCurrencyRate(), currency:{}, date:{}", currency, date);
        var rate = cbrRateService.getCurrencyRate(currency, date);
        log.info("CBR_CONTROLLER, rate:{}", rate);
        return rate;
    }
}
