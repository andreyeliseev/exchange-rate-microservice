package ru.eliseev.cbr_rate.model.exceptions;

public class CbrRateParsingException extends RuntimeException {
    public CbrRateParsingException(Exception ex) {
        super(ex);
    }
}
