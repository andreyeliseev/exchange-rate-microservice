package ru.eliseev.cbr_rate.model.exceptions;

public class RequesterException extends RuntimeException {
    public RequesterException(Exception ex) {
        super(ex);
    }
}
