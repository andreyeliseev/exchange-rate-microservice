package ru.eliseev.cbr_rate.model.exceptions;

public class CbrRateNotFoundException extends RuntimeException {
    public CbrRateNotFoundException(String s) {
        super(s);
    }
}
