package ru.eliseev.cbr_rate.model;

import lombok.Value;

import java.util.List;

@Value
public class CachedCbrRates {
    List<CbrRate> cbrRates;
}
