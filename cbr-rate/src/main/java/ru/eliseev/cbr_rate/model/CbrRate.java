package ru.eliseev.cbr_rate.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Builder
@AllArgsConstructor(onConstructor = @__(@JsonCreator))
@Value
public class CbrRate {
    String numCode;
    String charCode;
    String nominal;
    String name;
    String value;
}
