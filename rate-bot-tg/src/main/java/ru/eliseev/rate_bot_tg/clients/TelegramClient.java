package ru.eliseev.rate_bot_tg.clients;

import ru.eliseev.rate_bot_tg.model.dto.GetUpdatesRequest;
import ru.eliseev.rate_bot_tg.model.dto.GetUpdatesResponse;

public interface TelegramClient {
    GetUpdatesResponse getUpdates(GetUpdatesRequest request);
}
