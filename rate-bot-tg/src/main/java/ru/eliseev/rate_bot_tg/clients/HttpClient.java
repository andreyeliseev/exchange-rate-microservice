package ru.eliseev.rate_bot_tg.clients;

public interface HttpClient {
    String performRequest(String url, String params);
}
