package ru.eliseev.rate_bot_tg.model.exception;

public class TelegramException extends RuntimeException {
    public TelegramException(Throwable cause) {
        super(cause);
    }

    public TelegramException(String message) {
        super(message);
    }
}
