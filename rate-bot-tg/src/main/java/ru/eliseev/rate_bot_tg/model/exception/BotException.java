package ru.eliseev.rate_bot_tg.model.exception;

import com.fasterxml.jackson.core.JsonProcessingException;

public class BotException extends RuntimeException {
    public BotException(String message) {
        super(message);
    }

    public BotException(String message, Exception ex) {
        super(message, ex);
    }
}
