package ru.eliseev.rate_bot_tg.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class GetUpdatesRequest {
    @JsonProperty("offset")
    long offset;
}
