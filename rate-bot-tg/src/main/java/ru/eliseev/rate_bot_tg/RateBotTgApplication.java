package ru.eliseev.rate_bot_tg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RateBotTgApplication {

	public static void main(String[] args) {
		SpringApplication.run(RateBotTgApplication.class, args);
	}

}
