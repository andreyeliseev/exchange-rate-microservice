package ru.eliseev.rate_bot_tg.service;

import lombok.extern.slf4j.Slf4j;
import ru.eliseev.rate_bot_tg.clients.TelegramClient;
import ru.eliseev.rate_bot_tg.model.dto.GetUpdatesRequest;
import ru.eliseev.rate_bot_tg.model.dto.GetUpdatesResponse;

@Slf4j
public class TelegramServiceImpl implements TelegramService {

    private final TelegramClient telegramClient;
    private final MessageSender messageSender;
    private final LastUpdateIdKeeper lastUpdateIdKeeper;

    public TelegramServiceImpl(TelegramClient telegramClient,
                               MessageSender messageSender,
                               LastUpdateIdKeeper lastUpdateIdKeeper) {
        this.telegramClient = telegramClient;
        this.messageSender = messageSender;
        this.lastUpdateIdKeeper = lastUpdateIdKeeper;
    }

    @Override
    public void getUpdates() {
        try {
            log.info("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!TELEGRAM_SERVICE.getUpdates() begin");
            var offset = lastUpdateIdKeeper.get();
            var request = new GetUpdatesRequest(offset);
            var response = telegramClient.getUpdates(request);
            var lastUpdateId = processResponse(response);
            lastUpdateId = lastUpdateId == 0 ? offset : lastUpdateId + 1;
            lastUpdateIdKeeper.set(lastUpdateId);
            log.info("TELEGRAM_SERVICE.getUpdates() end, lastUpdateId: {}", lastUpdateId);
        } catch (Exception ex) {
            log.error("unhandled exception", ex);
        }

    }

    private long processResponse(GetUpdatesResponse response) {
        log.info("TELEGRAM_SERVICE_response.getResult().size:{}", response.getResult().size());
        long lastUpdateId = 0;
        for (var responseMsg : response.getResult()) {
            lastUpdateId = Math.max(lastUpdateId, responseMsg.getUpdateId());
            messageSender.send(responseMsg.getMessage());
        }
        log.info("lastUpdateId:{}", lastUpdateId);
        return lastUpdateId;
    }
}
