package ru.eliseev.rate_bot_tg.service;

import ru.eliseev.rate_bot_tg.model.dto.GetUpdatesResponse;

public interface MessageSender {
    void send(GetUpdatesResponse.Message message);
}
