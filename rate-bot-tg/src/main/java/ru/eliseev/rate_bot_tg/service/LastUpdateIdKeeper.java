package ru.eliseev.rate_bot_tg.service;

public interface LastUpdateIdKeeper {
    long get();

    void set(long lastUpdateId);
}
