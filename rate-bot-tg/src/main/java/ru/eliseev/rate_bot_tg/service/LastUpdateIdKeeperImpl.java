package ru.eliseev.rate_bot_tg.service;

import org.springframework.stereotype.Component;

@Component
public class LastUpdateIdKeeperImpl implements LastUpdateIdKeeper {
    private long lastUpdateId = 0;

    @Override
    public long get() {
        return lastUpdateId;
    }

    @Override
    public void set(long lastUpdateId) {
        this.lastUpdateId = lastUpdateId;
    }
}
