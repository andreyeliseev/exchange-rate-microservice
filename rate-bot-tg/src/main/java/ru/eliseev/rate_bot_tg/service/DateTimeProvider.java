package ru.eliseev.rate_bot_tg.service;

import java.time.LocalDateTime;

public interface DateTimeProvider {
    LocalDateTime get();
}
